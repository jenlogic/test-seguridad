<?php
class EjercicioDosUnDosTres {

	private $cadena;
	private $abc_values = array(
		"A" => 26,
		"B" => 25,
		"C" => 24,
		"D" => 23,
		"E" => 22,
		"F" => 21,
		"G" => 20,
		"H" => 19,
		"I" => 18,
		"J" => 17,
		"K" => 16,
		"L" => 15,
		"M" => 14,
		"N" => 13,
		"O" => 12,
		"P" => 11,
		"Q" => 10,
		"R" => 9,
		"S" => 8,
		"T" => 7,
		"U" => 6,
		"U" => 5,
		"W" => 4,
		"X" => 3,
		"Y" => 2,
		"Z" => 1,
	);
	public function __construct($cadena)
	{
		$this->cadena = strtoupper($cadena);
	}
	
	function imprime_equivalente() {
		
		
		$tamano = strlen($this->cadena);
		if($tamano <= 1) {
			return $this->abc_values[$this->cadena];
		}else {
			$valor_equivalente = 0;
			$valor_equivalente = (strlen($this->cadena) - 1) * 26;
			$valor_equivalente += $this->abc_values[substr($this->cadena, -1)];
			return $valor_equivalente;
		}
		
	}

}

$ejercicio2 = new EjercicioDosUnDosTres("ZN");
echo $ejercicio2->imprime_equivalente();