<?php
class EjercicioUnoUnDosTres {

	private $cadena;
	
	public function __construct($cadena)
	{
		$this->cadena = $cadena;
	}
	
	function imprime_cadena_cifrada() {
		$array_cadena = str_split($this->cadena);
		$salida_cadena = "";
		for($i = 0; $i < strlen($this->cadena); $i ++ ) {
			if (strpos($salida_cadena, $array_cadena[$i]) !== false) {
			}else {
				$salida_cadena .= $array_cadena[$i].substr_count($this->cadena, $array_cadena[$i]);
			}
		}
		return $salida_cadena;
	}

}


$ejercicio1 = new EjercicioUnoUnDosTres("information");
echo $ejercicio1->imprime_cadena_cifrada();